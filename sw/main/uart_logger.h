/**
 * @file
 *
 * @brief Header for main file
 */

#ifndef __UART_LOGGER_H__
#define __UART_LOGGER_H__
// ===============================| Includes |================================
#include "uart_logger_config.h"
// ================================| Defines |================================
#if CPU_CLK_INIT > XTAL_CLK
#define CLK_MULTIPLIER  (CPU_CLK_INIT/XTAL_CLK)
#else
#define CLK_MULTIPLIER  (XTAL_CLK/CPU_CLK_INIT)
#endif

#define GET_FREQ(hz)    (CLK_MULTIPLIER * CLK_DIV * (hz))

#define GET_TIME(time)  ((time) / (CLK_MULTIPLIER * CLK_DIV))

#define GET_RTOS_TIME_MS(ms)     \
    ((ms) / (portTICK_RATE_MS * CLK_MULTIPLIER * CLK_DIV))
#endif // __UART_LOGGER_H__

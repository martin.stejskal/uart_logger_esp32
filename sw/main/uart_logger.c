/**
 * @file
 *
 * @brief The main file for user application
 */

// ===============================| Includes |================================
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_log.h"
#include "driver/uart.h"
#include "soc/uart_struct.h"
#include "string.h"

// MSK
#include "uart_logger.h"
#include "soc/rtc.h"
#include "logger.h"
#include "logger_config.h"
// =============================| Sanity check |==============================
#if (CLK_DIV < 1)
#error "CLK_DIV can not be lower than 1!"
#endif
#if ((XTAL_CLK != (26 * MHZ)) && (XTAL_CLK != 40 * MHZ))
#warning "XTAL_CLK should be only 26 and 40 MHz. You can observe garbage on\
 UART when booting."
#endif
#if (CPU_CLK_INIT < (1 * KHZ))
#error "Who are you kidding? CPU_CLK_INIT should not be this low"
#endif
#if (XTAL_CLK < (1 * KHZ))
#error "Are you really think that setting XTAL_CLK this way is good idea?"
#endif
// ===============================| Functions |===============================

/**
 * @todo Add error code for this function
 */
void init() {
    esp_err_t eErr;

    // ============| Lower system clock to reduce power consumption |=========
    const rtc_cpu_freq_config_t rtc_cfg = { .div = CLK_DIV,
            .source_freq_mhz =
            XTAL_CLK, .freq_mhz = XTAL_CLK, .source =
                    RTC_CPU_FREQ_SRC_XTAL };

    // Indicate frequency change in log
    ESP_LOGI("Init", "Changing UART0 freq....\n");
    rtc_clk_cpu_freq_set_config(&rtc_cfg);
    // ===================| Change UART0 dividers accordingly |===============
    eErr = uart_set_baudrate(UART_NUM_0, GET_FREQ(UL_UI_BAUDRATE));
    if (eErr) {
        ESP_LOGE("Init", "UART0 freq change FAILED!!!\n");
    } else {
        ESP_LOGI("Init", "UART0 freq changed\n");
    }

    // =======================| Initialize logger module |====================
    LoggerInit();
}

static void RTOSLoggerTask() {
    while (1) {
        LoggerTask();
        vTaskDelay(2);  // Allow FreeRTOS to do something else
    }
}

/**
 * @brief User application main function
 */
void app_main() {
    init();

    /* Need to think about all "printf" and other things which will simply
     * consume quite a lot RAM. So that is why 2kB
     */
    xTaskCreate(RTOSLoggerTask, "logger_task", 1024 * 2,
    NULL, configMAX_PRIORITIES,
    NULL);
    /*
     xTaskCreate(rx_task, "uart_rx_task", 1024 * 2, NULL, configMAX_PRIORITIES,
     NULL);

     xTaskCreate(tx_task, "uart_tx_task", 1024 * 2, NULL,
     configMAX_PRIORITIES - 1, NULL);
     */
}

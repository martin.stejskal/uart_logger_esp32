/**
 * @file
 *
 * @brief Configuration file for UART logger
 */

#ifndef __UART_LOGGER_CONFIG_H__
#define __UART_LOGGER_CONFIG_H__

#ifndef KHZ
#define KHZ (1000)
#endif
#ifndef MHZ
#define MHZ (1000000)
#endif

/**
 * @brief Pin settings
 *
 * @{
 */
#define UL_LOGGER_TXD_PIN       GPIO_NUM_23
#define UL_LOGGER_RXD_PIN       GPIO_NUM_34

#define UL_LOGGER_SPI_MISO      GPIO_NUM_2
#define UL_LOGGER_SPI_MOSI      GPIO_NUM_15
#define UL_LOGGER_SPI_CLK       GPIO_NUM_14
#define UL_LOGGER_SPI_CS        GPIO_NUM_13
/**
 * @}
 */

/**
 * @brief UART baudrates
 *
 * {@
 */
#define UL_UI_BAUDRATE          115200
#define UL_LOG_BAUDRATE         115200
/**
 * @}
 */

/**
 * @brief UART interfaces
 * @{
 */
#define UL_LOG_UART_INTERFACE   UART_NUM_1
/**
 * @}
 */

/**
 * @brief Buffer size for UART logger periphery
 */
#define UL_LOG_RX_BUFFER_SIZE   (1024 * 64)

/**
 * @brief Define period after which is expected UART in idle
 *
 * Within this period UART driver will keep waiting for data. When no new data
 * received, buffer content is written to SD card
 */
#define UL_LOG_RX_PERIOD_MS     100

/**
 * @brief Clock settings
 *
 * @{
 */
/**
 * @brief Initial CPU clock
 *
 * This can be set via "menuconfig". Recommended to use lowest possible value
 */
#define CPU_CLK_INIT    (80 * MHZ)
/**
 * @brief Crystal clock
 *
 * Usually 40 MHz or 26 MHz.
 */
#define XTAL_CLK        (40 * MHZ)

/**
 * @brief Divider used for reducing power consumption
 *
 * Result clock is XTAL_CLK divided by this value. Higher value means lower
 * power consumption, but lower CPU power. The 10 is quite good balance
 * between power consumption and performance.
 */
#define CLK_DIV         20
/**
 * @}
 */

#endif // __UART_LOGGER_CONFIG_H__

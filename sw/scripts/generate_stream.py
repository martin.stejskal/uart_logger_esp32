#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
========================
Random stream generator
========================

Simple generator which produce given average throughput. Also allows to
generate "bursts" which might test another system capability.

``author`` Martin Stejskal

``Created``  2019.04.04

``Modified`` 2019.04.10

"""
import random
import sys
# Sometimes we need sleep and know system time
from time import sleep, perf_counter
# Work with UART
import serial

# Serial interface
itf='/dev/ttyUSB1'
speed=115200

def show_help():
    print('Usage:\n  {} [average throughput in Bytes per seconds] '
          '[burst size in Bytes]\n'
          'If throughput is not specified, default 1 kB is used.\n'
          'If throughput is not specified, no bursts are here. Throughput\n'
          'will be constant\n'
          'If also burst is defined, script automatically calculate burst'
          'duration'.format(sys.argv[0]))
# /show_help()

def get_random_data(size, new_line_column=80):
    payload = ''
    column = 1
    for idx in range(size):
        # If there is already N characters, put new line
        if(column >= new_line_column):
            char="\n"
            column = 0
        else:
            char = random.choice(ran_alphabet)
        payload = "{}{}".format(payload, char)
        column = column + 1
    # /generate payload
    return(payload)
# /get_random_data()

# Main function
if __name__ == "__main__":
    # Load parameters and check them
    ran_alphabet="abcdefghijklmnopqrstuvwxyz0123456789"\
                 "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    new_line_column=80
    
    tput=1024
    
    uart = serial.Serial(itf, speed, timeout=1)

    if(len(sys.argv) > 1):
        try:
            if(sys.argv[1] == "--help"):
                show_help()
                exit(0)
            tput=int(sys.argv[1])
        except ValueError as e:
            msg = 'Throughput value have to be integer! The "{}" is invalid!'\
                  ''.format(sys.argv[1])
            raise ValueError(msg)
        print("User defined throughput: {} Bps".format(tput))
    if(len(sys.argv) > 2):
        try:
            burst=int(sys.argv[2])
        except ValueError as e:
            msg = 'Burst value have to be integer! The "{}" is invalid!'\
                  ''.format(sys.argv[2])
            raise ValueError(msg)
        print("User defined burst: {} Bytes".format(burst))
    else:
        # User did not defined burst size
        burst=tput
    # /Set parameters
    # Additional parameter check
    if(burst > tput):
        msg = "Burst have to be smaller than average throughput!\n"\
              "Reason is fact that if burst is bigger then average throughput"\
              "it should not be possible to transmit such amount of data" 
        raise ValueError(msg)
    # /parameter check
    
    # Period between bursts in seconds
    burst_period = burst/tput
    print("Burst period: {} s".format(burst_period))
    while(True):
        time_begin = perf_counter()
        # Generate burst payload. Minus one because of newline
        payload = get_random_data(size=burst - 1,
                                  new_line_column=new_line_column)
        uart.write(bytearray("{}\n".format(payload), 'UTF-8'))
        print(payload)
        time_duration = perf_counter() - time_begin
        sleep_time = burst_period-time_duration
        if(sleep_time > 0):
            sleep(sleep_time)
    

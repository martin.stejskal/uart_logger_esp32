#!/bin/bash
# Script which prepare environment, install dependencies under virtual
# environment and run application.
#
# Created:  2019.03.16
# Modified: 2019.04.14
# Author:   Martin Stejskal

# ESP32 system variable
export IDF_PATH="/home/$USER/esp/esp-idf"

# Add ESP toolchain to the PATH
PATH="$PATH:/home/$USER/esp/xtensa-esp32-elf/bin:"$IDF_PATH"/tools"


if [ ! -d venv ] ; then
    echo "Folder venv does not exist. Creating...."
    virtualenv -p python3 venv &&
    # Activate environment
    source venv/bin/activate &&
    # And install packages
    pip3 install -r requirements.txt
else
    echo "Folder venv already exist. No need to recreate"
    # Activate environment
    source venv/bin/activate
fi

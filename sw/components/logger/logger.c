/**
 * @file
 *
 * @brief Logger module
 *
 * This module log data from UART to flash memory
 */

// ===============================| Includes |================================
// Project includes
#include "logger.h"
#include "logger_config.h"

// Standard ESP32 libraries
#include <driver/uart.h>
#include <esp_system.h>
#include <esp_log.h>
#include <esp_vfs_fat.h>
#include <driver/sdmmc_host.h>
#include <driver/sdspi_host.h>
#include <sdmmc_cmd.h>

#include <string.h>

// ==========================| Configuration check |==========================
// ===================| Global variables for this module |====================
/**
 * @brief Filename
 *
 * Dimension should fit 16bit number, which should be OK from practical point
 * of view.
 */
static char cFilename[] = "/sdcard/log_65535.txt";
// ===============================| Functions |===============================
/**
 * @brief Initialize peripheries
 * @return ESP_OK if no problem detected.
 */
eLoggerStatus LoggerInit(void) {
    static const char *LOGGER_INIT_TAG = "LoggerInit";
    esp_err_t eErr;

    // =================================| UART |==============================
    const uart_config_t uart_config =
            { .baud_rate = LOGGER_UART_BAUDRATE, .data_bits = UART_DATA_8_BITS,
                    .parity = UART_PARITY_DISABLE,
                    .stop_bits = UART_STOP_BITS_1, .flow_ctrl =
                            UART_HW_FLOWCTRL_DISABLE };
    eErr = uart_param_config(LOGGER_UART_INTERFACE, &uart_config);
    if (eErr) {
        ESP_LOGE(LOGGER_INIT_TAG, "UART configuration failed");
        return LOGGER_UART_ERROR;
    }

    eErr = uart_set_pin(LOGGER_UART_INTERFACE, LOGGER_TXD_PIN, LOGGER_RXD_PIN,
    UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
    if (eErr) {
        ESP_LOGE(LOGGER_INIT_TAG, "UART pin set fail");
        return LOGGER_UART_ERROR;
    }

    eErr = uart_driver_install(LOGGER_UART_INTERFACE, LOGGER_RX_BUFFER_SIZE, 0,
            0, NULL, 0);
    if (eErr) {
        ESP_LOGE(LOGGER_INIT_TAG, "UART driver install failed: %d", eErr);
        return LOGGER_UART_ERROR;
    }

    // ==================================| SPI |==============================

    sdmmc_host_t host = SDSPI_HOST_DEFAULT();
    sdspi_slot_config_t slot_config = SDSPI_SLOT_CONFIG_DEFAULT();
    slot_config.gpio_miso = LOGGER_SPI_MISO;
    slot_config.gpio_mosi = LOGGER_SPI_MOSI;
    slot_config.gpio_sck = LOGGER_SPI_CLK;
    slot_config.gpio_cs = LOGGER_SPI_CS;

    // Options for mounting the filesystem.
    // If format_if_mount_failed is set to true, SD card will be partitioned and
    // formatted in case when mounting fails.
    esp_vfs_fat_sdmmc_mount_config_t mount_config = { .format_if_mount_failed =
    false, .max_files = 5, .allocation_unit_size = 16 * 1024 };

    sdmmc_card_t* card;
    esp_err_t ret = esp_vfs_fat_sdmmc_mount("/sdcard", &host, &slot_config,
            &mount_config, &card);

    if (ret != ESP_OK) {
        if (ret == ESP_FAIL) {
            ESP_LOGE(LOGGER_INIT_TAG,
                    "Failed to mount filesystem. "
                    "If you want the card to be formatted, set"
                    " format_if_mount_failed = true.");
        } else {
            ESP_LOGE(LOGGER_INIT_TAG,
                    "Failed to initialize the card (%s). "
                    "Make sure SD card lines have pull-up resistors in place.",
                    esp_err_to_name(ret));
        }
        return LOGGER_SD_CARD_INIT_ERROR;
    }

    // Card has been initialized, print its properties
    sdmmc_card_print_info(stdout, card);

    // Find appropriate log number based on already presented files on card
    // Assume that number "0" is "forbidden"
    ESP_LOGD(LOGGER_INIT_TAG, "Searching suitable filename...");
    uint16_t u16Num = 1;
    FILE *f;
    int iErrCode;
    while (1) {
        iErrCode = sprintf(&cFilename[0], "/sdcard/log_%d.txt", u16Num);
        // Check error code - should not fail, but ...
        if (iErrCode < 0) {
            return LOGGER_ERROR;
        }

        // Try to open file. If not found -> can log.
        f = fopen(&cFilename[0], "r");
        if (f == 0) {
            ESP_LOGI(LOGGER_INIT_TAG, "Logging to %s file", &cFilename[0]);
            fclose(f);
            break;
        } else {
            fclose(f);
            ESP_LOGD(LOGGER_INIT_TAG, "File %s found", &cFilename[0]);
        }

        // Increase counter. If reached zero, "all" filenames are already used
        u16Num++;
        if (u16Num == 0) {
            /**
             * @todo Turn on error LED
             */
            ESP_LOGE(LOGGER_INIT_TAG,
                    "All filenames used :( Delete some log files and retry");
            return LOGGER_NO_SUITABLE_FILENAME_FOUND;
        }
    }

    ESP_LOGD(LOGGER_INIT_TAG, "Initialization done (^_^)");
    return LOGGER_OK;
}

void LoggerTask(void) {
    // Define name in logger
    static const char *LOGGER_TASK_TAG = "LoggerTask";

    esp_log_level_set(LOGGER_TASK_TAG, ESP_LOG_INFO);
    ESP_LOGD(LOGGER_TASK_TAG, "Running task...");

    // Keep data in static variable (all time there on same place)
    static uint8_t au8Data[LOGGER_RX_BUFFER_SIZE + 1] = {0};

    const int rxBytes = uart_read_bytes(LOGGER_UART_INTERFACE, &au8Data[0],
    LOGGER_RX_BUFFER_SIZE, LOGGER_RX_PERIOD_MS);
    if (rxBytes > 0) {
        au8Data[rxBytes] = 0;
        ESP_LOGD(LOGGER_TASK_TAG, "Read %d bytes: '%s'", rxBytes, &au8Data[0]);
        ESP_LOG_BUFFER_HEXDUMP(LOGGER_TASK_TAG, &au8Data[0], rxBytes,
                ESP_LOG_DEBUG);
        // Write to flash
        ESP_LOGI(LOGGER_TASK_TAG, "Writing to flash...");
        FILE *f = fopen(&cFilename[0], "a+");
        if (f == 0) {
            /**
             * @todo Turn on error LED
             * @todo Stay in error state
             */
            fclose(f);
            ESP_LOGE(LOGGER_TASK_TAG, "Failed to open file %s", &cFilename[0]);
            return;
        }
        /**
         * @todo Use different function which writes bytes, not just ASCII
         */
        fprintf(f, "%s", &au8Data[0]);
        fclose(f);

        ESP_LOGI(LOGGER_TASK_TAG, " [DONE]");
    }
}

//vTaskDelay((2000 / portTICK_PERIOD_MS) / (CLK_DIV * 2));

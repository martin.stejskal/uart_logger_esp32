/**
 * @file
 *
 * @brief Logger module
 *
 * This module log data from UART to flash memory
 */
#ifndef __LOGGER_H__
#define __LOGGER_H__

// ========================| Data types, structures |=========================
typedef enum {
    LOGGER_OK,
    LOGGER_ERROR,
    LOGGER_UART_ERROR,
    LOGGER_SD_CARD_INIT_ERROR,
    LOGGER_NO_SUITABLE_FILENAME_FOUND,
    LOGGER_OPEN_FILE_ERROR,

} eLoggerStatus;

// ===============================| Functions |===============================
eLoggerStatus LoggerInit(void);

void LoggerTask(void);
#endif // __LOGGER_H__

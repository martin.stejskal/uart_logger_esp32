/**
 * @file
 *
 * @brief Configuration file for logger
 */
#ifndef __LOGGER_CONFIG_H__
#define __LOGGER_CONFIG_H__
// ===============================| Includes |================================
// Overtake most/all settings from main configuration file
#include "uart_logger.h"
// ===============================| Settings |================================
/**
 * @brief RX buffer size in Bytes
 *
 * Data have to be received in some buffer before written to flash. Not always
 * this buffer have to be filled completely, but it should be big enough to
 * "catch" all data while CPU doing something else.
 */
#define LOGGER_RX_BUFFER_SIZE       (UL_LOG_RX_BUFFER_SIZE)

/**
 * @brief Define period for which task will try to read batch of data
 *
 * When called function for reading UART, it is not completely blocking. It
 * will wait some time for activity and then exit. This value define that
 * timeout.
 */
#define LOGGER_RX_PERIOD_MS         (GET_RTOS_TIME_MS(UL_LOG_RX_PERIOD_MS))
/* This is for "automatic" delay.
 * Main idea here is to capture as much as buffer can, relatively to baudrate.
 *    (GET_RTOS_TIME_MS((1000 * LOGGER_RX_BUFFER_SIZE)/(UL_LOG_BAUDRATE/8)))
 */

/**
 * @brief UART settings for logger module
 *
 * @{
 */
#define LOGGER_UART_INTERFACE       (UL_LOG_UART_INTERFACE)
#define LOGGER_UART_BAUDRATE        (GET_FREQ(UL_LOG_BAUDRATE))

#define LOGGER_TXD_PIN              (UL_LOGGER_TXD_PIN)
#define LOGGER_RXD_PIN              (UL_LOGGER_RXD_PIN)

/**
 * @}
 */

/**
 * @brief SPI settings for SD card
 *
 * Due to some issues, MMC mode is not used, although it could be faster.
 * Also some GPIO are already wired on development board.
 *
 * @{
 */
#define LOGGER_SPI_MISO         UL_LOGGER_SPI_MISO
#define LOGGER_SPI_MOSI         UL_LOGGER_SPI_MOSI
#define LOGGER_SPI_CLK          UL_LOGGER_SPI_CLK
#define LOGGER_SPI_CS           UL_LOGGER_SPI_CS
/**
 * @}
 */
#endif

EESchema Schematic File Version 4
LIBS:uart_logger_ESP32-00-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RF_Module:ESP32-WROOM-32 U?
U 1 1 5CB2424E
P 4400 3150
F 0 "U?" H 4400 4728 50  0000 C CNN
F 1 "ESP32-WROOM-32" H 4400 4637 50  0000 C CNN
F 2 "RF_Module:ESP32-WROOM-32" H 4400 1650 50  0001 C CNN
F 3 "https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32_datasheet_en.pdf" H 4100 3200 50  0001 C CNN
	1    4400 3150
	1    0    0    -1  
$EndComp
$Comp
L Connector:Micro_SD_Card J?
U 1 1 5CB24396
P 7250 3800
F 0 "J?" H 7200 4517 50  0000 C CNN
F 1 "Micro_SD_Card" H 7200 4426 50  0000 C CNN
F 2 "" H 8400 4100 50  0001 C CNN
F 3 "http://katalog.we-online.de/em/datasheet/693072010801.pdf" H 7250 3800 50  0001 C CNN
	1    7250 3800
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 5CB24648
P 6550 2550
F 0 "J?" H 6629 2542 50  0000 L CNN
F 1 "sniffer" H 6629 2451 50  0000 L CNN
F 2 "" H 6550 2550 50  0001 C CNN
F 3 "~" H 6550 2550 50  0001 C CNN
	1    6550 2550
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J?
U 1 1 5CB246CE
P 6550 2050
F 0 "J?" H 6629 2092 50  0000 L CNN
F 1 "programming" H 6629 2001 50  0000 L CNN
F 2 "" H 6550 2050 50  0001 C CNN
F 3 "~" H 6550 2050 50  0001 C CNN
	1    6550 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 2150 5400 2150
Text Label 5050 2150 0    50   ~ 0
SPI_MISO
Wire Wire Line
	5000 2850 5400 2850
Text Label 5050 2850 0    50   ~ 0
SPI_MOSI
Wire Wire Line
	5000 2750 5400 2750
Text Label 5050 2750 0    50   ~ 0
SPI_CLK
Wire Wire Line
	5000 2650 5400 2650
Text Label 5050 2650 0    50   ~ 0
SPI_CS
$Comp
L power:GND #PWR?
U 1 1 5CB2480C
P 4400 4550
F 0 "#PWR?" H 4400 4300 50  0001 C CNN
F 1 "GND" H 4405 4377 50  0000 C CNN
F 2 "" H 4400 4550 50  0001 C CNN
F 3 "" H 4400 4550 50  0001 C CNN
	1    4400 4550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5CB2482A
P 6350 4000
F 0 "#PWR?" H 6350 3750 50  0001 C CNN
F 1 "GND" V 6355 3872 50  0000 R CNN
F 2 "" H 6350 4000 50  0001 C CNN
F 3 "" H 6350 4000 50  0001 C CNN
	1    6350 4000
	0    1    1    0   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5CB2486C
P 4400 1750
F 0 "#PWR?" H 4400 1600 50  0001 C CNN
F 1 "+3.3V" H 4415 1923 50  0000 C CNN
F 2 "" H 4400 1750 50  0001 C CNN
F 3 "" H 4400 1750 50  0001 C CNN
	1    4400 1750
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5CB2489E
P 6350 3800
F 0 "#PWR?" H 6350 3650 50  0001 C CNN
F 1 "+3.3V" V 6365 3928 50  0000 L CNN
F 2 "" H 6350 3800 50  0001 C CNN
F 3 "" H 6350 3800 50  0001 C CNN
	1    6350 3800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6350 3900 5900 3900
Text Label 5900 3900 0    50   ~ 0
SPI_CLK
Wire Wire Line
	6350 3700 5900 3700
Text Label 5900 3700 0    50   ~ 0
SPI_MOSI
Wire Wire Line
	6350 4100 5900 4100
Text Label 5900 4100 0    50   ~ 0
SPI_MISO
Wire Wire Line
	6350 3600 5900 3600
Text Label 5900 3600 0    50   ~ 0
SPI_CS
Wire Wire Line
	5000 2250 5400 2250
Text Label 5050 2250 0    50   ~ 0
RX0
Wire Wire Line
	5000 2050 5400 2050
Text Label 5050 2050 0    50   ~ 0
TX0
$Comp
L power:GND #PWR?
U 1 1 5CB3898B
P 6300 2200
F 0 "#PWR?" H 6300 1950 50  0001 C CNN
F 1 "GND" H 6305 2027 50  0000 C CNN
F 2 "" H 6300 2200 50  0001 C CNN
F 3 "" H 6300 2200 50  0001 C CNN
	1    6300 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5CB389E4
P 6300 2700
F 0 "#PWR?" H 6300 2450 50  0001 C CNN
F 1 "GND" H 6305 2527 50  0000 C CNN
F 2 "" H 6300 2700 50  0001 C CNN
F 3 "" H 6300 2700 50  0001 C CNN
	1    6300 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 2650 6300 2650
Wire Wire Line
	6300 2650 6300 2700
Wire Wire Line
	6350 2150 6300 2150
Wire Wire Line
	6300 2150 6300 2200
Wire Wire Line
	6350 2550 6300 2550
Wire Wire Line
	6350 1950 6300 1950
Wire Wire Line
	6350 2050 6300 2050
Text Label 6300 2050 2    50   ~ 0
RX0
Text Label 6300 1950 2    50   ~ 0
TX0
Wire Wire Line
	5000 4150 5400 4150
Text Label 5050 4150 0    50   ~ 0
SNFR_RX
Text Label 6300 2550 2    50   ~ 0
SNFR_RX
$EndSCHEMATC

# About
 * This is simple UART logger. Connect to predefined pins
   (`uart_logger_config.h`) your device, insert SD card & connect, power on.
   Logger will start capture data to SD card.
 * After every power on a new log file is created -> you do not need to worry
   about re-writing older logs.

# Build & flash
 * Move to [sw](sw) directory
   * Use standard process for ESP32.
   * Or run `source source_me.sh` and run `compile_and_run.sh` script to make
     it easy way ;)

# Notes for developers
 * Original `uart.c` from ESP IDF had to be changed in order to "react faster"
   on incoming payload. This will allow to handle big data bursts even if core
   is underclocked. To be more specific:

```c
#define UART_FULL_THRESH_DEFAULT  (64) // Original value: 128
```

 * Whole system is intentionally underclocked. Reason is power consumption.
   The `CLK_DIV` in `uart_logger_config.h` define additional divider from
   crystal clock. Therefore macro like `GET_FREQ`, `GET_TIME` or
   `GET_RTOS_TIME_MS` should be used to get correct timing after
   underclocking.
